using System;
using System.Reflection;
using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces
{
    [ServiceDependency]
    public interface IMessengerClient
    {
        void Connect();
        Task ConnectAsync();
        Task<MessageResponse> ReceiveToAsync(string queue, Guid correlationId, int timeout, string session);
        MessageResponse ReceiveTo(string queue, Guid correlationId, int timeout, string session);
        Task SendAsync(MessageRequest request);
        void Send(MessageRequest request);
        Task CloseAsync();
        void Close();
        void SetUserId(Guid userId);
        string GetQueue(Type type, MethodInfo method, bool useArguments);
        string GetOperationContractName(MethodInfo method);
        bool IsConnected();
        
    }
}