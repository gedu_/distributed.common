using System.Collections;

namespace Distributed.Common.Interfaces
{
    public interface IServiceOperationList : IEnumerable
    {
        bool Add(ServiceOperationItem item);
        void AddRange(IServiceOperationList items);
        IServiceOperationContainer GetContainer();
        ServiceOperationItem Current();
        bool MoveNext();
        void Reset();
    }
}