using System;
using System.Reflection;
using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces
{
    [ServiceDependency]
    public interface IMessengerServer
    {
        void Connect();
        Task ConnectAsync();
        void Init();
        Task InitAsync();
        Task SendAsync(MessageResponse response, string session);
        void Send(MessageResponse response, string session);
        Task CloseAsync();
        void Close();
        string GetQueue(Type type, MethodInfo method, bool useArguments);
        bool IsConnected();
        
    }
}