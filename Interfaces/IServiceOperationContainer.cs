using System.Collections;
using System.Reflection;

namespace Distributed.Common.Interfaces
{
    public interface IServiceOperationContainer : IEnumerable
    {
        bool MoveNext();
        void Reset();
        ServiceOperationItem Current();
        ServiceOperationItem GetItem(string iface, string method, string methodName);
        ServiceOperationItem GetItem(MethodInfo method);
    }
}