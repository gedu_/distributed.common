using System;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces 
{
    [ServiceDependency]
    public interface ICallProxy
    {
        object CreateProxy(Type type);
        void SetUserId(Guid id);
    }
}