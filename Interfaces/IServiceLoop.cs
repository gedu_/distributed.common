using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces 
{
    [ServiceDependency]
    public interface IServiceLoop
    {
        void Run();
        Task RunAsync();

        void Stop();
        Task StopAsync();
    }
}