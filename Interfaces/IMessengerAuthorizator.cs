using System.Threading.Tasks;
using System;
using Distributed.Common.Attributes;
using System.Collections.Generic;

namespace Distributed.Common.Interfaces
{
    [ServiceDependency]
    public interface IMessengerAuthorizator
    {
        bool Authorize(Guid sessionId, IList<Guid> permissions);
        Task<bool> AuthorizeAsync(Guid sessionId, IList<Guid> permissions);
    }
}
