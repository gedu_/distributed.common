using System;
using System.Collections;
using System.Collections.Generic;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces
{
    [ServiceDependency]
    public interface IServiceIdsContainer : IEnumerable
    {
        bool MoveNext();
        void Reset();
        KeyValuePair<string, Guid> Current();
        Guid this[string index] { get; }
        bool ContainsKey(string key);
        bool IsServiceIdsUsed();
    }
}