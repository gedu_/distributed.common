using System;
using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Common.Interfaces
{
    [ServiceDependency]
    public interface IPubSubMessenger
    {
        void Publish(string topic, object data, string queue = null);

        Task PublishAsync(string topic, object data, string queue = null);

        Guid Subscribe(string topic, Action<PubSubMessage> action, bool consume = false, string queue = null);

        Task<Guid> SubscribeAsync(string topic, Action<PubSubMessage> action, bool consume = false, string queue = null);

        void Unsubscribe(Guid subscriptionId);

        Task UnsubscribeAsync(Guid subscriptionId);
        
    }
}
