# Distributed.Common

Version 1.3.1

## About

Distributed is a C# microservices library built with extendability/replaceability in mind, all library components can be replaced with custom implementations. Uses [AMQPNetLite](https://www.nuget.org/packages/AMQPNetLite/) library and AMQP 1.0 standard by default.

## Requirements

+ Dotnet Core 1.0+ / NetStandard 1.6

+ Message broker with AMQP 1.0 support

## Features

+ Proxy class generation at runtime 

+ Two-way or One-way communication

+ Pub/Sub

+ Built-in authorization 

+ Modular, extendable/replaceable components 

## Links

Documentation can be found in [Wiki](https://gitlab.com/gedu_/distributed.common/wikis/home)   
Package available on [NuGet](https://www.nuget.org/packages/Distributed.Common/1.3.1)  
Reference Back-end project can be found [here](https://gitlab.com/gedu_/distributed.reference.backend)   
Reference Front-end project can be found [here](https://gitlab.com/gedu_/distributed.reference.frontend)   
End-to-end tests project can be found [here](https://gitlab.com/gedu_/distributed.testing.e2e)   