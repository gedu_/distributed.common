using System;
using Distributed.Common.Enums;

namespace Distributed.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class ServiceDependencyOverrideAttribute : Attribute
    {       
        private ServiceOverride _override;
        public ServiceOverride Value {
            get {
                return _override;
            }  
        }

        public ServiceDependencyOverrideAttribute(ServiceOverride value)
        {
            _override = value;
        }

        public ServiceDependencyOverrideAttribute()
        {
            _override = ServiceOverride.None;
        }
    }
}