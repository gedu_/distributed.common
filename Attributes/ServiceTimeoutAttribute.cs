using System;

namespace Distributed.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class ServiceTimeoutAttribute : Attribute
    {       
        private int _timeout;
        public int Timeout {
            get {
                return _timeout;
            }  
        }

        public ServiceTimeoutAttribute(int timeout)
        {
            _timeout = timeout;
        }
    }
}