using System;
using System.Collections.Generic;

namespace Distributed.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class ServiceAuthorizeAttribute : Attribute
    {       
        private List<Guid> _permissions;
        public List<Guid> Value {
            get {
                return _permissions;
            }  
        }

        public ServiceAuthorizeAttribute(params string[] permissions)
        {
            _permissions = new List<Guid>();
            
            foreach(var permission in permissions)
            {
                _permissions.Add(Guid.Parse(permission));
            }
        }

        public ServiceAuthorizeAttribute()
        {
            _permissions = new List<Guid>();
        }

    }
}