using System;
using Microsoft.Extensions.DependencyInjection;

namespace Distributed.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Interface, Inherited = false, AllowMultiple = true)]
    public class ServiceDependencyAttribute : Attribute
    {       
        private ServiceLifetime _lifetime;
        public ServiceLifetime Value {
            get {
                return _lifetime;
            }  
        }

        public ServiceDependencyAttribute(ServiceLifetime value)
        {
            _lifetime = value;
        }
        public ServiceDependencyAttribute()
        {
            _lifetime = ServiceLifetime.Singleton;
        }
    }
}