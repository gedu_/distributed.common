using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Amqp;
using Amqp.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Distributed.Common
{
    public class AmqpMessengerBase {
        protected Connection _connection;
        protected ConcurrentDictionary<string, Session> _sessions;
        protected Address _address;
        protected ILogger _logger;
        protected string _connectionString;
        protected ConcurrentDictionary<string, ReceiverLink> _receivers;

        public AmqpMessengerBase(IConfigurationRoot config, ILogger logger)
        {
            _logger = logger;
            _connectionString = config.GetConnectionString("amqp");
            _address = new Address(_connectionString);
            _receivers = new ConcurrentDictionary<string, ReceiverLink>();
            _sessions = new ConcurrentDictionary<string, Session>();
        }

        public void Connect()
        {
            
            if(_connection == null || _connection.IsClosed)
            {
                _connection = new Connection(_address);
                _logger.LogDebug($"Succesfully connected to {_connectionString}.");
            }
        }

        public async Task ConnectAsync()
        {
            if(_connection == null || _connection.IsClosed)
            {
                _connection = await Connection.Factory.CreateAsync(_address);
                _logger.LogDebug($"Succesfully connected to {_connectionString}.");
            }
        }

        public void Close()
        {
            if(_connection != null && !_connection.IsClosed)
            {
                foreach (var receiver in _receivers)
                {
                    receiver.Value.Close();
                }

                foreach (var session in _sessions)
                {
                    session.Value.Close();
                }

                _connection.Close();
            }
        }

        public async Task CloseAsync()
        {
            if(_connection != null && !_connection.IsClosed)
            {
                foreach(var receiver in _receivers)
                {
                    await receiver.Value.CloseAsync();
                }

                foreach (var session in _sessions)
                {
                    await session.Value.CloseAsync();
                }

                await _connection.CloseAsync();
            }
        }
        protected void CheckConnectivity()//(string sessionName)
        {
            if(_connection == null || _connection.IsClosed)
            {
                _connection = new Connection(_address);
            }

        }

        protected void CheckSession(string session)
        {
            if(!_sessions.ContainsKey(session) || _sessions[session].IsClosed)
            {
                _sessions[session] = new Session(_connection);
            }
        }
        
        protected async Task CheckConnectivityAsync()//(string sessionName)
        {
            if(_connection == null || _connection.IsClosed)
            {
                _connection = await Connection.Factory.CreateAsync(_address);
            }

        }

        protected Task CheckSessionAsync(string session)
        {
            if(!_sessions.ContainsKey(session) || _sessions[session].IsClosed)
            {
                _sessions[session] = new Session(_connection);
            }

            return Task.CompletedTask;
        }

        protected object[] DeserializeArguments(MethodInfo method, List<object> arguments)
        {
            var args = new List<object>();
                        
            var serializer = new AmqpSerializer();
            var methods = typeof(AmqpSerializer).GetMethods();
            var deserializeMethod = methods.Where(x => x.Name == "ReadObject").FirstOrDefault();
            var methodParams = method.GetParameters();

            for(var i = 0; i < methodParams.Length; i++)
            {
                var deserializeType = methodParams[i].ParameterType;

                if (deserializeType == typeof(decimal) || deserializeType == typeof(decimal?))
                {
                    deserializeType = typeof(string);
                }

                var genericMethod = deserializeMethod.MakeGenericMethod(new Type[] { deserializeType } );
                var buf = new ByteBuffer(1024, true);
                serializer.WriteObject(buf, arguments[i]);

                var deserialized = genericMethod.Invoke(serializer, new object[] { buf });

                if (methodParams[i].ParameterType == typeof(decimal))
                {
                    deserialized = Convert.ToDecimal((string)deserialized);
                }
                else if (methodParams[i].ParameterType == typeof(decimal?))
                {
                    if ((string)deserialized == "null")
                    {
                        deserialized = null;
                    }
                    else
                    {
                        deserialized = Convert.ToDecimal((string)deserialized);
                    }
                }

                args.Add(deserialized);
            } 
            
            return args.ToArray();
        }

        protected object DeserializeType(Type type, object obj)
        {  
            if(type == typeof(Task))
            {
                return obj;
            }
            
            var serializer = new AmqpSerializer();
            var methods = typeof(AmqpSerializer).GetMethods();
            var deserializeMethod = methods.Where(x => x.Name == "ReadObject").FirstOrDefault();
            var deserializeType = type;


            if (type == typeof(decimal))
            {
                deserializeType = typeof(string);
            }
            else if (type == typeof(decimal?))
            {
                deserializeType = typeof(string);
            }

            var genericMethod = deserializeMethod.MakeGenericMethod(new Type[] { deserializeType } );


            var buf = new ByteBuffer(1024, true);
            serializer.WriteObject(buf, obj);

            var result = genericMethod.Invoke(serializer, new object[] { buf });

            if (type == typeof(decimal))
            {
                return Convert.ToDecimal((string)result);
            }
            else if (type == typeof(decimal?))
            {
                if (((string)result) == "null")
                {
                    return null;
                }
                
                return Convert.ToDecimal((string)result);
            }

            return result;
        }

        public string GetQueue(Type type, MethodInfo method, bool useArguments = false)
        {
            if (useArguments)
            {
                var parameters = "";
                foreach(var parameter in method.GetParameters())
                {
                    parameters = string.Concat(new string[] { parameters, $"{parameter.ParameterType}_{parameter.Name}," } );
                }

                parameters = method.GetParameters().Length > 0 ? parameters.Substring(0, parameters.Length-1) : parameters;
                var result = $"{type.FullName}.{method.Name}({parameters})";

                return result.ToUpper();
            }

            return $"{type.FullName}.{method.Name}".ToUpper();
        }

        public bool IsConnected()
        {
            return !((_connection == null) || (_connection != null && _connection.IsClosed));
        }
    }
}