using System;
using Amqp.Serialization;

namespace Distributed.Common
{
    [AmqpContract]
    public class MessageResponse
    {
        [AmqpMember]
        public Guid CorrelationId { get; set; }
        [AmqpMember]
        public string Queue { get; set; }
        [AmqpMember]
        public string Type { get; set; }
        [AmqpMember]
        public object Object { get; set; }

        public override string ToString()
        {
            var ret = $"CorrelationId: {CorrelationId};\n"
                    + $"Queue: {Queue};\n"
                    + $"Type: {Type};\n"
                    + $"Object: {Object};\n";

            return ret;
        }
    }
}