using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Distributed.Common.Attributes;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Distributed.Common
{
    public class CallProxy : DispatchProxy, ICallProxy
    {
        private static IMessengerClient _messenger;
        private static ILogger _logger;
        private static ConcurrentDictionary<string, MessageResponse> _responses;
        private static int _configTimeout;
        private static IServiceIdsContainer _serviceIds;
        private static bool _useArguments;

        public CallProxy()
        {

        }

        public CallProxy(IMessengerClient messenger, ILogger logger, IConfigurationRoot config, IServiceIdsContainer serviceIds)
        {
            _messenger = messenger;
            _logger = logger;
            _responses = new ConcurrentDictionary<string, MessageResponse>();
            _configTimeout = Convert.ToInt32(config.GetSection("ServiceTimeout").Value);
            _serviceIds = serviceIds;
            _useArguments = Convert.ToBoolean(config.GetSection("UseArguments").Value);
        }

        public object CreateProxy(Type type)
        {
            var methods = typeof(DispatchProxy).GetMethods();
            var createMethod = methods.Where(x => x.Name == "Create").FirstOrDefault();
            var genericMethod = createMethod.MakeGenericMethod(new Type[] { type, typeof(CallProxy) } );
            return genericMethod.Invoke(null, null);
        }

        public static Task<T> CastToTask<T>(object o)
        {
            return Task.FromResult((T)o);
        }

        protected object Callback(string replyTo, int timeout, Guid correlationId, MethodInfo targetMethod, string queue, string session)
        {
            _responses.TryAdd(replyTo, _messenger.ReceiveTo(replyTo, correlationId, timeout, session));  

            if(_responses.ContainsKey(replyTo) && _responses[replyTo] != null)
            {
                var response = _responses[replyTo];

                if(targetMethod.ReturnType == typeof(Task) || targetMethod.ReturnType.GetTypeInfo().BaseType == typeof(Task))
                {
                    if(targetMethod.ReturnType.GetGenericArguments().Length > 0)
                    {
                        var methods = typeof(CallProxy).GetMethods();
                        var castMethod = methods.Where(x => x.Name == "CastToTask").FirstOrDefault();
                        var genericMethod = castMethod.MakeGenericMethod(new Type[] { targetMethod.ReturnType.GetGenericArguments()[0] } );
                        
                        return genericMethod.Invoke(null, new object[] { response.Object });
                    }

                    return Task.CompletedTask;
                }

                var returnType = Type.GetType(response.Type);
                var underlyingType = Nullable.GetUnderlyingType(returnType);

                if (underlyingType != null)
                {
                    if (response.Object == null)
                    {
                        return null;
                    }

                    return Convert.ChangeType(response.Object, underlyingType);
                }
                
                return Convert.ChangeType(response.Object, returnType);
            }
            else
            {
                return new TimeoutException($"No response from queue {queue}");
            }              
        }

        protected override object Invoke(MethodInfo targetMethod, object[] args)
        {
            var replyTo = Guid.NewGuid().ToString();
            try
            {
                var corrId = Guid.NewGuid();
                var ifaceType = targetMethod.DeclaringType;
                var queue = _messenger.GetQueue(ifaceType, targetMethod, _useArguments);
                var methodName = _messenger.GetOperationContractName(targetMethod);

                var request = new MessageRequest()
                {
                    CorrelationId = corrId,
                    Interface = ifaceType.FullName,
                    Method = targetMethod.Name,
                    Arguments = TransformIncompatibleTypes(targetMethod.GetParameters(), args),
                    Queue = queue,
                    ReplyTo = replyTo,
                    MethodName = methodName
                };

                if(_serviceIds.IsServiceIdsUsed())
                {
                    if(!_serviceIds.ContainsKey(ifaceType.FullName))
                    {
                        throw new ArgumentException($"ServiceIds does not contain key {ifaceType.FullName}.");
                    }

                    request.ServiceId = _serviceIds[ifaceType.FullName];
                }

                var timeout = _configTimeout;
                var serviceTimeout = targetMethod.GetCustomAttribute(typeof(ServiceTimeoutAttribute));

                if(serviceTimeout != null)
                {
                    timeout = ((ServiceTimeoutAttribute)serviceTimeout).Timeout;
                }

                _messenger.Send(request);
                
                return Task.Factory.StartNew(() => Callback(replyTo, timeout, corrId, targetMethod, queue, ifaceType.FullName)).Result;
            }
            catch (Exception e)
            {
                _logger.LogError("Exception: " + e);
                if (e is TargetInvocationException && e.InnerException != null)
                {
                    return e.InnerException;
                }

                return e;
            }     
            finally
            {
                if(_responses.ContainsKey(replyTo))
                {
                    MessageResponse val;
                    _responses.TryRemove(replyTo, out val);
                }
            }    
        }

        private List<object> TransformIncompatibleTypes(ParameterInfo[] parameters, object[] args)
        {
            var arguments = new List<object>(args);
            
            for (var i = 0; i < arguments.Count; i++)
            {
                var nulalbleType = Nullable.GetUnderlyingType(parameters[i].ParameterType);

                if (parameters[i].ParameterType == typeof(decimal))
                {
                    arguments[i] = arguments[i].ToString();
                }
                else if (nulalbleType != null && nulalbleType == typeof(decimal))
                {
                    if (arguments[i] != null)
                    {
                        arguments[i] = arguments[i].ToString();
                    }
                    else{
                        arguments[i] = "null";
                    }
                }
            }

            return arguments;
        }

        public void SetUserId(Guid id)
        {
            _messenger.SetUserId(id);
        }
    }
}