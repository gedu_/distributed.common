using System;
using System.Threading.Tasks;
using Amqp;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Collections.Concurrent;
using System.Linq;

namespace Distributed.Common
{
    public class AmqpPubSubMessenger : AmqpMessengerBase, IPubSubMessenger
    {
        private string _pubSubQueue;
        private ConcurrentDictionary<Guid, Subscriber> _subscribers;
        private BlockingCollection<Guid> _releasedMessages;
        private int _releasedMessagesRemovalTimer;
        public AmqpPubSubMessenger(IConfigurationRoot config, ILogger logger) : base(config, logger)
        {
            _subscribers = new ConcurrentDictionary<Guid, Subscriber>();
            _releasedMessages = new BlockingCollection<Guid>();
            _releasedMessagesRemovalTimer = 100000;
            _pubSubQueue = config.GetSection("PubSubQueue").Value;
        }

        public void Publish(string topic, object data, string queue)
        {
            if(queue == null)
            {
                queue = _pubSubQueue;
            }

            var queueName = GetQueueName(queue, topic);

            CheckConnectivity();

            CheckSession(queueName);

            var sender = new SenderLink(_sessions[queueName], $"Publisher_{Guid.NewGuid()}", queueName);
            var correlationId = Guid.NewGuid();

            try {
                var pubsubMessage = new PubSubMessage()
                {
                    CorrelationId = correlationId,
                    Topic = topic,
                    Queue = queue,
                    Message = data,
                    CreationTime = DateTime.Now
                };

                var message = new Message(pubsubMessage);

                sender.Send(message);

                _logger.LogDebug($"Succesfully published {data} to topic {topic} in queue {queue}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error publishing {data} to topic {topic} in queue {queue} [CorrelationId: {correlationId}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            sender.Close();
            _sessions[queueName].Close();

        }

        public async Task PublishAsync(string topic, object data, string queue)
        {
            if(queue == null)
            {
                queue = _pubSubQueue;
            }

            var queueName = GetQueueName(queue, topic);

            await CheckConnectivityAsync();
            await CheckSessionAsync(queueName);


            var sender = new SenderLink(_sessions[queueName], $"Publisher_{Guid.NewGuid()}", queueName);
            var correlationId = Guid.NewGuid();

            try {
                var pubsubMessage = new PubSubMessage()
                {
                    CorrelationId = correlationId,
                    Topic = topic,
                    Queue = queue,
                    Message = data,
                    CreationTime = DateTime.Now
                };

                var message = new Message(pubsubMessage);

                await sender.SendAsync(message);

                _logger.LogDebug($"Succesfully published {data} to topic {topic} in queue {queue}");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error publishing {data} to topic {topic} in queue {queue} [CorrelationId: {correlationId}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            await sender.CloseAsync();
            await _sessions[queueName].CloseAsync();
        }

        public void Callback(ReceiverLink link, Message message)
        {
            var body = message.GetBody<PubSubMessage>();

            if(!_releasedMessages.Contains(body.CorrelationId))
            {
                CheckConnectivity();
                CheckSession(GetQueueName(body.Queue, body.Topic));

                var isConsuming = _subscribers.Where(x => x.Value.Queue == body.Queue && x.Value.Topic == body.Topic
                    && x.Value.Consume).Any();

                var subscribers = _subscribers.Where(x => x.Value.Queue == body.Queue && x.Value.Topic == body.Topic)
                    .Select(x => Task.Run( () => x.Value.Action(body) )).ToList();
                
                Task.WhenAll(subscribers).Wait();

                if (isConsuming)
                {
                    link.Accept(message);
                    return;
                }

                link.Release(message);
                _releasedMessages.Add(body.CorrelationId);

                // Wait for x seconds and then remove from the list
                Task.Factory.StartNew(async () => {
                    await Task.Delay(_releasedMessagesRemovalTimer);
                    var correlationId = Guid.Empty;
                    _releasedMessages.TryTake(out correlationId);
                });
            }
            else
            {
                //Should only get it after everyone else got it
                link.Accept(message);
            }
        }
        public Guid Subscribe(string topic, Action<PubSubMessage> action, bool consume, string queue)
        {
            if(queue == null)
            {
                queue = _pubSubQueue;
            }

            var queueName = GetQueueName(queue, topic);

            CheckConnectivity();
            CheckSession(queueName);

            _logger.LogDebug($"Subscribing to topic {topic} in queue {queue} / {queueName}");

            if(!_receivers.ContainsKey(queueName))
            {
                
                var receiver = new ReceiverLink(_sessions[queueName], $"{queueName}.{Guid.NewGuid()}", queueName);

                receiver.Start(200, Callback);
                _receivers.TryAdd(queueName, receiver);
            }

            var newSubscriber = new Subscriber()
            {
                Topic = topic,
                Queue = queue,
                Action = action,
                Consume = consume
            };

            var subscriptionId = Guid.NewGuid();

            _subscribers.TryAdd(subscriptionId, newSubscriber);

            return subscriptionId;
        }

        public async void CallbackAsync(ReceiverLink link, Message message)
        {
            var body = message.GetBody<PubSubMessage>();

            if(!_releasedMessages.Contains(body.CorrelationId))
            {
                await CheckConnectivityAsync();
                await CheckSessionAsync(GetQueueName(body.Queue, body.Topic));

                var isConsuming = _subscribers.Where(x => x.Value.Queue == body.Queue && x.Value.Topic == body.Topic
                    && x.Value.Consume).Any();

                var subscribers = _subscribers.Where(x => x.Value.Queue == body.Queue && x.Value.Topic == body.Topic)
                    .Select(x => Task.Run( () => x.Value.Action(body) )).ToList();
                
                await Task.WhenAll(subscribers);

                if (isConsuming)
                {
                    link.Accept(message);
                    return;
                }

                link.Release(message);

                _releasedMessages.Add(body.CorrelationId);

                //Wait for x seconds and then remove from the list
                await Task.Factory.StartNew(async () => {
                    await Task.Delay(_releasedMessagesRemovalTimer);
                    var correlationId = Guid.Empty;
                    _releasedMessages.TryTake(out correlationId);
                });
            }
            else
            {
                link.Accept(message);
            }
        }

        public async Task<Guid> SubscribeAsync(string topic, Action<PubSubMessage> action, bool consume, string queue)
        {
            if(queue == null)
            {
                queue = _pubSubQueue;
            }

            var queueName = GetQueueName(queue, topic);

            await CheckConnectivityAsync();
            await CheckSessionAsync(queueName);

            _logger.LogDebug($"Subscribing to topic {topic} in queue {queue} / {queueName}");

            if(!_receivers.ContainsKey(queueName))
            {

                var receiver = new ReceiverLink(_sessions[queueName], $"{queueName}.{Guid.NewGuid()}", queueName);

                receiver.Start(200, CallbackAsync);

                _receivers.TryAdd(queueName, receiver);
            }

            var newSubscriber = new Subscriber()
            {
                Topic = topic,
                Queue = queue,
                Action = action,
                Consume = consume
            };

            var subscriptionId = Guid.NewGuid();

            _subscribers.TryAdd(subscriptionId, newSubscriber);

            return subscriptionId;
        }

        public void Unsubscribe (Guid subscriptionId)
        {
            if (_subscribers.ContainsKey(subscriptionId))
            {                
                Subscriber subscriberResult;

                _subscribers.TryRemove(subscriptionId, out subscriberResult);

                //TODO: fix this

                // var subscribersLeft = _subscribers.Any(x => x.Value.Queue == subscriberResult.Queue && x.Value.Topic == subscriberResult.Topic);

                // if (!subscribersLeft)
                // {
                //     ReceiverLink receiverResult;

                //     _receivers.TryRemove(GetQueueName(subscriberResult.Queue, subscriberResult.Topic), out receiverResult);

                //     receiverResult?.Close();
                // }
            }
        }

        public Task UnsubscribeAsync (Guid subscriptionId)
        {
            if (_subscribers.ContainsKey(subscriptionId))
            {
                Subscriber subscriberResult;

                _subscribers.TryRemove(subscriptionId, out subscriberResult);

                //TODO: fix this
                // var subscribersLeft = _subscribers.Any(x => x.Value.Queue == subscriberResult.Queue && x.Value.Topic == subscriberResult.Topic);

                // if (!subscribersLeft)
                // {
                //     ReceiverLink receiverResult;

                //     _receivers.TryRemove(GetQueueName(subscriberResult.Queue, subscriberResult.Topic), out receiverResult);

                //     await receiverResult?.CloseAsync();
                // }
            }

            return Task.CompletedTask;
        }

        private string GetQueueName(string queue, string topic)
        {
            return $"{queue}_{topic}".ToUpper();
        }
    }
}