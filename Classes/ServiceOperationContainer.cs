using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Distributed.Common.Interfaces;

namespace Distributed.Common
{
    public class ServiceOperationContainer : IServiceOperationContainer
    {
        private List<ServiceOperationItem> _storage;
        public ServiceOperationContainer(List<ServiceOperationItem> storage)
        {
            _storage = storage;
        }

        public ServiceOperationItem Current()
        {
            return _storage.GetEnumerator().Current;
        }

        public ServiceOperationItem GetItem(string iface, string method, string methodName)
        {
            return _storage.Where(x => x.Interface.FullName == iface && x.Method.Name == method && x.MethodName == methodName).FirstOrDefault();
        }

        public ServiceOperationItem GetItem(MethodInfo method)
        {
            return _storage.Where(x => x.Method == method).FirstOrDefault();
        }

        public IEnumerator GetEnumerator()
        {
            return _storage.GetEnumerator();
        }

        public bool MoveNext()
        {
            return _storage.GetEnumerator().MoveNext();
        }

        public void Reset()
        {
            
        }
    }
}