using System;
using System.Reflection;

namespace Distributed.Common
{
    public class ServiceOperationItem
    {
        public Type Interface;
        public MethodInfo Method;
        public int Timeout;
        public bool IsOneWay;
        public string MethodName;
    }
}