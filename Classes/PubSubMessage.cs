using System;
using Amqp.Serialization;

namespace Distributed.Common
{
    [AmqpContract]
    public class PubSubMessage
    {
        [AmqpMember]
        public Guid CorrelationId;
        [AmqpMember]
        public string Topic;
        [AmqpMember]
        public string Queue;
        [AmqpMember]
        public object Message;
        [AmqpMember]
        public DateTime CreationTime;
        

    }
}