using System;
using System.Collections.Generic;
using Amqp.Serialization;

namespace Distributed.Common
{
    [AmqpContract]
    public class MessageRequest
    {
        [AmqpMember]
        public Guid CorrelationId { get; set; }
        [AmqpMember]
        public string Interface { get; set; }
        [AmqpMember]
        public string Method { get; set; }
        [AmqpMember]
        public string MethodName { get; set; }
        [AmqpMember]
        public List<object> Arguments { get; set; }
        [AmqpMember]
        public string Queue { get; set; }
        [AmqpMember]
        public string ReplyTo { get; set; }
        [AmqpMember]
        public Guid UserId { get; set; }
        [AmqpMember]
        public Guid ServiceId { get; set; }

        public override string ToString()
        {
            var ret = $"CorrelationId: {CorrelationId};\n"
                    + $"Interface: {Interface};\n"
                    + $"Method: {Method};\n"
                    + $"MethodName: {MethodName};\n"
                    + "Arguments:\n";
            foreach(var item in Arguments)
            {
                ret += $"{item.GetType()} - {item};\n";
            }

            ret += $"Queue: {Queue};\n"
                 + $"ReplyTo: {ReplyTo};\n";

            return ret;
        }

    }
}