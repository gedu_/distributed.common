using System.Collections;
using System.Collections.Generic;
using Distributed.Common.Interfaces;

namespace Distributed.Common
{
    public class ServiceOperationList : IServiceOperationList
    {
        private List<ServiceOperationItem> _storage;
        public ServiceOperationList()
        {
            _storage = new List<ServiceOperationItem>();
        }

        public bool Add(ServiceOperationItem item)
        {
            if(!_storage.Contains(item))
            {
                _storage.Add(item);
                return true;
            }
            return false;
        }

        public void AddRange(IServiceOperationList items)
        {
            foreach(ServiceOperationItem item in items)
            {
                _storage.Add(item);
            }
        }

        public ServiceOperationItem Current()
        {
            return _storage.GetEnumerator().Current;
        }

        public IServiceOperationContainer GetContainer()
        {
            return new ServiceOperationContainer(_storage);
        }

        public IEnumerator GetEnumerator()
        {
            return _storage.GetEnumerator();
        }

        public bool MoveNext()
        {
            return _storage.GetEnumerator().MoveNext();
        }

        public void Reset()
        {
            
        }
    }
}