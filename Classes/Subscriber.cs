using System;

namespace Distributed.Common
{
    public class Subscriber
    {
        public string Topic;
        public string Queue;
        public Action<PubSubMessage> Action;
        public bool Consume;
    }
}