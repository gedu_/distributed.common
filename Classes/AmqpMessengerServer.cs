using System;
using System.Threading.Tasks;
using Amqp;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Collections.Concurrent;
using System.Linq;
using Distributed.Common.Attributes;
using System.Reflection;

namespace Distributed.Common
{
    public class AmqpMessengerServer : AmqpMessengerBase, IMessengerServer
    {
        private ConcurrentDictionary<Guid, string> _replies;
        private IServiceOperationContainer _serviceContainer;
        private IServiceProvider _serviceProvider;
        private IServiceIdsContainer _serviceIds;
        private bool _useArguments;

        public AmqpMessengerServer(IConfigurationRoot config, ILogger logger, IServiceOperationContainer serviceContainer, 
            IServiceProvider serviceProvider, IServiceIdsContainer serviceIds) : base(config, logger)
        {
            _logger = logger;
            _connectionString = config.GetConnectionString("amqp");
            _address = new Address(_connectionString);
            _replies = new ConcurrentDictionary<Guid, string>();
            _serviceIds = serviceIds;
            _serviceContainer = serviceContainer;
            _serviceProvider = serviceProvider;
            _useArguments = Convert.ToBoolean(config.GetSection("UseArguments").Value);
        }

        public void Callback(ReceiverLink link, Message message)
        {               
            var messageRequest = message.GetBody<MessageRequest>();

            if(messageRequest.ServiceId == Guid.Empty || (_serviceIds.ContainsKey(messageRequest.Interface) 
                && messageRequest.ServiceId == _serviceIds[messageRequest.Interface]))
            {

                link.Accept(message);
                _logger.LogDebug($"New message to queue {link.Name} [{messageRequest.CorrelationId}]");

                _replies.TryAdd(messageRequest.CorrelationId, messageRequest.ReplyTo);
                var serviceOperation = _serviceContainer.GetItem(messageRequest.Interface, messageRequest.Method, messageRequest.MethodName);
                var method = serviceOperation.Method;

                var response = new MessageResponse() 
                {
                    CorrelationId = messageRequest.CorrelationId,
                    Queue = messageRequest.Queue,
                    Type = method.ReturnType.AssemblyQualifiedName
                };

                if((method.ReturnType == typeof(Task) || method.ReturnType.GetTypeInfo().BaseType == typeof(Task))
                    && method.ReturnType.GetGenericArguments().Length != 0)
                {
                    response.Type = method.ReturnType.GetGenericArguments()[0].AssemblyQualifiedName;
                }
                
                //Authorization failed returning ServiceException
                var authorization = Authorize(method, messageRequest.UserId);

                if(authorization != null)
                {
                    response.Type = authorization.GetType().ToString();
                    response.Object = authorization;
                    Task.Factory.StartNew(() => Send(response, messageRequest.Interface));
                    return;
                }

                var obj = _serviceProvider.GetService(serviceOperation.Interface);                       

                try {
                    object result = null;
                    var assignable = method.ReturnType.IsAssignableFrom(typeof(Task));

                    if(method.ReturnType == typeof(Task) || method.ReturnType.GetTypeInfo().BaseType == typeof(Task))
                    {
                        var task = method.Invoke(obj, DeserializeArguments(method, messageRequest.Arguments)) as Task;
                            
                        if (serviceOperation.IsOneWay)
                        {
                            Task.Factory.StartNew(() => task.Wait());
                        }
                        else 
                        {
                            task.Wait();

                            if(method.ReturnType.GetGenericArguments().Length != 0)
                            {
                                var resultProperty = task.GetType().GetProperty("Result");
                                result = resultProperty.GetValue(task);
                            }          
                        }
                                      
                    }
                    else {
                        result = method.Invoke(obj, DeserializeArguments(method, messageRequest.Arguments));
                    }
                    
                    response.Object = result;
                }
                catch(Exception e)
                {                   
                    _logger.LogError(new EventId(), e, $"Error trying execute queue {link.Name}");   
                    response.Type = e.InnerException.GetType().ToString(); 
                    response.Object = e.InnerException;
                }
                finally
                {
                    _logger.LogDebug($"Sending response to queue {link.Name} [{messageRequest.CorrelationId}]");
                    Task.Factory.StartNew(() => Send(response, messageRequest.Interface));
                }
            } 
            else 
            {
                link.Release(message);
            }
        }

        public async void CallbackAsync(ReceiverLink link, Message message)
        {

            var messageRequest = message.GetBody<MessageRequest>();

            if(messageRequest.ServiceId == Guid.Empty || (_serviceIds.ContainsKey(messageRequest.Interface) 
                && messageRequest.ServiceId == _serviceIds[messageRequest.Interface]))
            {

                link.Accept(message);
                _logger.LogDebug($"New message to queue {link.Name} [{messageRequest.CorrelationId}]");

                _replies.TryAdd(messageRequest.CorrelationId, messageRequest.ReplyTo);
                var serviceOperation = _serviceContainer.GetItem(messageRequest.Interface, messageRequest.Method, messageRequest.MethodName);
                var method = serviceOperation.Method;

                var response = new MessageResponse() 
                {
                    CorrelationId = messageRequest.CorrelationId,
                    Queue = messageRequest.Queue,
                    Type = method.ReturnType.AssemblyQualifiedName
                };

                if((method.ReturnType == typeof(Task) || method.ReturnType.GetTypeInfo().BaseType == typeof(Task))
                    && method.ReturnType.GetGenericArguments().Length != 0)
                {
                    response.Type = method.ReturnType.GetGenericArguments()[0].AssemblyQualifiedName;
                }
                
                //Authorization failed returning ServiceException
                var authorization = await AuthorizeAsync(serviceOperation.Method, messageRequest.UserId);

                if(authorization != null)
                {
                    response.Type = authorization.GetType().ToString();
                    response.Object = authorization;
                    await Task.Factory.StartNew(async () => await SendAsync(response, messageRequest.Interface));
                    return;
                }


                var obj = _serviceProvider.GetService(serviceOperation.Interface);

                try {
                    object result = null;
                    var assignable = method.ReturnType.IsAssignableFrom(typeof(Task));

                    if(method.ReturnType == typeof(Task) || method.ReturnType.GetTypeInfo().BaseType == typeof(Task))
                    {
                        var task = serviceOperation.Method.Invoke(obj, DeserializeArguments(serviceOperation.Method, messageRequest.Arguments)) as Task;

                        if (serviceOperation.IsOneWay)
                        {
                            await Task.Factory.StartNew(async () => await task);
                        }
                        else 
                        {
                            await task;
                            
                            var resultProperty = task.GetType().GetProperty("Result");
                            result = resultProperty.GetValue(task);
                        }
                    }
                    else {
                        result = serviceOperation.Method.Invoke(obj, DeserializeArguments(serviceOperation.Method, messageRequest.Arguments));
                    }
                    
                    response.Object = result;
                }
                catch(Exception e)
                {                   
                    _logger.LogError(new EventId(), e, $"Error trying execute queue {link.Name}");   
                    response.Type = e.InnerException.GetType().ToString(); 
                    response.Object = e.InnerException;
                }
                finally
                {
                    _logger.LogDebug($"Sending response to queue {link.Name} [{messageRequest.CorrelationId}]");
                    await Task.Factory.StartNew(async () => await SendAsync(response, messageRequest.Interface));
                }
            } 
            else 
            {
                link.Release(message);
            }
        }

        public void Init()
        {
            CheckConnectivity();

            _receivers.Clear();

            foreach(ServiceOperationItem service in _serviceContainer)
            {
                var queue = GetQueue(service.Interface, service.Method, _useArguments);

                if (!_receivers.ContainsKey(queue))
                {
                    _logger.LogDebug($"Creating queue {queue} ...");

                    var session = service.Interface.ToString();

                    if (!_sessions.ContainsKey(session))
                    {
                        _sessions.TryAdd(session, new Session(_connection));
                    }

                    var receiver = new ReceiverLink(_sessions[session], queue, queue);

                    receiver.Start(200, Callback);
                    _receivers.TryAdd(queue, receiver);
                }
            }
        }

        public async Task InitAsync()
        {
            await CheckConnectivityAsync();

            _receivers.Clear();


            foreach(ServiceOperationItem service in _serviceContainer)
            {
                var queue = GetQueue(service.Interface, service.Method, _useArguments);
                if (!_receivers.ContainsKey(queue))
                {
                    _logger.LogDebug($"Creating queue {queue} ...");

                    var session = service.Interface.ToString();

                    if (!_sessions.ContainsKey(session))
                    {
                        _sessions.TryAdd(session, new Session(_connection));
                    }

                    var receiver = new ReceiverLink(_sessions[session], queue, queue);

                    receiver.Start(200, CallbackAsync);
                    _receivers.TryAdd(queue, receiver);
                }
            }
        }

        public void Send(MessageResponse response, string session)
        {
            CheckConnectivity();

            CheckSession(session);

            if(!_replies.ContainsKey(response.CorrelationId))
            {
                throw new ArgumentException($"CorrelationId {response.CorrelationId} not found.");
            }

            //TODO: Some separate session for senders !
            var sender = new SenderLink(_sessions[session], $"Sender_{Guid.NewGuid()}", _replies[response.CorrelationId]);

            try {
                sender.Send(new Message(TransformIncompatibleTypes(response)));
                _logger.LogDebug($"Succesfully sent response [{response.CorrelationId}]");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error sending response from queue {response.Queue} [CorrelationId: {response.CorrelationId}]"
                    + $"[Queue: {_replies[response.CorrelationId]}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            sender.Close();

            string val; 
            _replies.TryRemove(response.CorrelationId, out val);
        }

        public async Task SendAsync(MessageResponse response, string session)
        {
            await CheckConnectivityAsync();

            await CheckSessionAsync(session);

            if(!_replies.ContainsKey(response.CorrelationId))
            {
                throw new ArgumentException($"CorrelationId {response.CorrelationId} not found.");
            }

            //TODO: Some separate session for senders !
            var sender = new SenderLink(_sessions[session], $"Sender_{Guid.NewGuid()}", _replies[response.CorrelationId]);

            try {
                await sender.SendAsync(new Message(TransformIncompatibleTypes(response)));
                _logger.LogDebug($"Succesfully sent response [{response.CorrelationId}]");
            }
            catch (Exception e)
            {
                _logger.LogError($"Error sending response from queue {response.Queue} [CorrelationId: {response.CorrelationId}]"
                    + $"[Queue: {_replies[response.CorrelationId]}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            await sender.CloseAsync();

            string val; 
            _replies.TryRemove(response.CorrelationId, out val);
        }

        private MessageResponse TransformIncompatibleTypes(MessageResponse response)
        {
            var type = Type.GetType(response.Type);

            var nullable = Nullable.GetUnderlyingType(type);

            if (type == typeof(decimal))
            {
                response.Object = response.Object.ToString();
            }
            else if (nullable != null && nullable == typeof(decimal))
            {
                if (response.Object != null)
                {
                    response.Object = response.Object.ToString();
                }
                else {
                    response.Object = "null";
                }
            }


            return response;
        }

        private ServiceException Authorize(MethodInfo method, Guid sessionId)
        {
            var authorizeAttribute = method.GetCustomAttribute(typeof(ServiceAuthorizeAttribute));

            if(authorizeAttribute != null)
            {
                var authorizator = (IMessengerAuthorizator)_serviceProvider.GetService(typeof(IMessengerAuthorizator));

                var permissions = ((ServiceAuthorizeAttribute)authorizeAttribute).Value;
                
                if(!authorizator.Authorize(sessionId, permissions))
                {
                    var permissionsString = permissions.Select(x => $"[{x}]").Aggregate((i, j) => $"{i}, {j}");

                    _logger.LogDebug($"Authorization has been denied for sessionId - {sessionId} with permissions - {permissionsString}");
                    return new ServiceException($"Authorization has been denied for sessionId - {sessionId} with permissions - {permissionsString}");
                }
            }

            return null;
        }

        private async Task<ServiceException> AuthorizeAsync(MethodInfo method, Guid sessionId)
        {
            var authorizeAttribute = method.GetCustomAttribute(typeof(ServiceAuthorizeAttribute));

            if(authorizeAttribute != null)
            {
                var authorizator = (IMessengerAuthorizator)_serviceProvider.GetService(typeof(IMessengerAuthorizator));

                var permissions = ((ServiceAuthorizeAttribute)authorizeAttribute).Value;
                
                if(!(await authorizator.AuthorizeAsync(sessionId, permissions)))
                {
                    var permissionsString = permissions.Select(x => $"[{x}]").Aggregate((i, j) => $"{i}, {j}");

                    _logger.LogDebug($"Authorization has been denied for sessionId - {sessionId} with permissions - {permissionsString}");
                    return new ServiceException($"Authorization has been denied for sessionId - {sessionId} with permissions - {permissionsString}");
                }
            }

            return null;
        }
    }
}