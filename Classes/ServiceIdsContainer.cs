using System;
using System.Collections;
using System.Collections.Generic;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Distributed.Common
{
    public class ServiceIdsContainer : IServiceIdsContainer
    {
        private Dictionary<string, Guid> _storage;
        private bool _serviceIdsUsed;
        public ServiceIdsContainer(IConfigurationRoot config)
        {
            _serviceIdsUsed = Convert.ToBoolean(config.GetSection("UseServiceIds").Value);
            _storage = new Dictionary<string, Guid>();

            foreach(var service in config.GetSection("ServiceIds").GetChildren())
            {
                _storage.Add(service.Key, new Guid(service.Value));
            }
        }

        public KeyValuePair<string, Guid> Current()
        {
            return _storage.GetEnumerator().Current;
        }

        public IEnumerator GetEnumerator()
        {
            return _storage.GetEnumerator();
        }

        public bool MoveNext()
        {
            return _storage.GetEnumerator().MoveNext();
        }

        public void Reset()
        {
            
        }

        public bool ContainsKey(string key)
        {
            return _storage.ContainsKey(key);
        }

        public bool IsServiceIdsUsed()
        {
            return _serviceIdsUsed;
        }

        public Guid this[string index]
        {
            get
            {
                return _storage[index];
            }
        }
    }
}