using System;
using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;
using System.Threading;

namespace Distributed.Common
{
    class ServiceLoop : IServiceLoop, IDisposable
    {
        private ILogger _logger;
        private bool _runningAsync;
        private IMessengerServer _messenger;
        private EventWaitHandle _waitHandle;
        public ServiceLoop(ILogger logger, IMessengerServer messenger)
        {
            _logger = logger;
            _runningAsync = false;
            _messenger = messenger;
            _waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
        }

        public void Run()
        {
            _logger.LogDebug("Starting service loop ...");

            _messenger.Connect();
            _messenger.Init();

            Console.CancelKeyPress += new ConsoleCancelEventHandler((sender, args) => {
                _logger.LogDebug("Stopping service loop ...");
                Dispose(true);
            });

            _waitHandle.WaitOne();   
        }

        public async Task RunAsync()
        {
            _logger.LogDebug("Starting service loop ...");
            _runningAsync = true;
            await _messenger.ConnectAsync();
            await _messenger.InitAsync();

            Console.CancelKeyPress += new ConsoleCancelEventHandler(async (sender, args) => {
                _logger.LogDebug("Stopping service loop ...");
                await DisposeAsync(true);
            });

            _waitHandle.WaitOne();         
        }

        public Task StopAsync()
        {
            _logger.LogDebug("Stopping service loop ...");

            _waitHandle.Set();

            return DisposeAsync(true);
        }

        public void Stop()
        {
            _logger.LogDebug("Stopping service loop ...");

            _waitHandle.Set();

            Dispose(true);
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual async Task DisposeAsync(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    await _messenger.CloseAsync();
                }

                disposedValue = true;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _messenger.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            if(_runningAsync)
            {
                DisposeAsync(true).Wait();
            }
            else {
                Dispose(true);
            }
            
        }
        #endregion
    }
}