using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using System.Reflection;
using System.Linq;
using Distributed.Common.Attributes;
using Distributed.Common.Interfaces;
using Distributed.Common.Enums;
using System.Collections.Generic;
using System.ServiceModel;

namespace Distributed.Common
{
    public static class ServiceCollectionExtensions
    {
        private static ILogger _logger;
        private static IConfigurationRoot _config;
        
        private static Type GetOverride(List<Type> types, string iface)
        {
            var overrides = types.Where(x => x.GetTypeInfo().GetCustomAttribute(typeof(ServiceDependencyOverrideAttribute)) != null).ToList();

            if(overrides.Count > 1)
            {
                var userOverrides = overrides.Where(x => ServiceOverride.User == 
                    ((ServiceDependencyOverrideAttribute)
                        x.GetTypeInfo().GetCustomAttribute(typeof(ServiceDependencyOverrideAttribute))).Value).ToList();

                if(userOverrides.Count > 1)
                {
                    throw new ArgumentException($"More than one implementation with ServiceDependencyOverride.User found"
                        + $" for interface {iface}: {types.Select(x => x.FullName).Aggregate((i, j) => i + "; " + j)}");
                }
                else if(userOverrides.Count == 1)
                {
                    return userOverrides.First();
                }
                else
                {
                    throw new ArgumentException($"No implementation with ServiceDependencyOverride.User found"
                        + $" for interface {iface}: {types.Select(x => x.FullName).Aggregate((i, j) => i + "; " + j)}");
                }
            }
            else if(overrides.Count == 1) 
            {
                return overrides.First();
            }
            else {
                throw new ArgumentException($"No implementations with attribute ServiceDependencyOverride found for"
                    + $" interface {iface}: {types.Select(x => x.FullName).Aggregate((i, j) => i + "; " + j)}");
            }
        }

        private static Type GetDepedency(List<Type> types, string iface)
        {
            if(types.Count > 1)
            {
                return GetOverride(types, iface);
            }
            else if(types.Count == 1)
            {
                return types.First();
            }
            else 
            {
                throw new ArgumentException($"Failed to find implementation for dependency {iface}");
            }
        }

        public static IServiceCollection RegisterServiceDependencies(this IServiceCollection serviceCollection, bool useTrustedUser)
        {
            if(_logger == null) {
                throw new ArgumentException("ILogger implementation not found. You have to call one of WithLogger methods before"
                    + " calling this method.");
            }

            var runtimeLibs = DependencyContext.Default.RuntimeLibraries;
            var assemblies = new Dictionary<string, Assembly>();
            var notFoundAssemblies = new List<string>();

            foreach(var lib in runtimeLibs)
            {
                foreach(var dependency in lib.Dependencies)
                {
                    try {
                        if(!notFoundAssemblies.Contains(dependency.Name) && !assemblies.ContainsKey(dependency.Name))
                        {
                            assemblies.Add(dependency.Name, Assembly.Load(new AssemblyName(dependency.Name)));
                        }
                    }
                    catch
                    {
                        notFoundAssemblies.Add(dependency.Name);
                    }
                    
                }
            }

            var currentAssembly = Assembly.GetEntryAssembly();
            assemblies.Add(currentAssembly.GetName().Name, currentAssembly);            

            var dependencies = new List<Type>();
            var contracts = new List<Type>();

            foreach(var assembly in assemblies)
            {
                var types = assembly.Value.GetTypes();
                dependencies.AddRange(types.Where(y => y.GetTypeInfo().IsInterface && 
                    y.GetTypeInfo().GetCustomAttribute(typeof(ServiceDependencyAttribute)) != null).ToList());

                contracts.AddRange(types.Where(y => y.GetTypeInfo().IsInterface && 
                    y.GetTypeInfo().GetCustomAttribute(typeof(ServiceContractAttribute)) != null).ToList());
            }

            var ignoredDependencies = _config.GetSection("IgnoredDependencies").GetChildren();

            foreach(var dependency in dependencies)
            {
                if(!ignoredDependencies.Where(x => x.Value == dependency.FullName).Any())
                {
                    var implementations = new List<Type>();
                    foreach(var assembly in assemblies)
                    {
                        implementations.AddRange(assembly.Value.DefinedTypes.Where(x => dependency.IsAssignableFrom(x.AsType()) 
                            && x.IsClass).Select(y => y.AsType()).ToList());
                    }
                
                    var type = GetDepedency(implementations, dependency.FullName);
                    var lifetime = ((ServiceDependencyAttribute)dependency.GetTypeInfo().GetCustomAttribute(typeof(ServiceDependencyAttribute))).Value;
                    serviceCollection.Add(new ServiceDescriptor(dependency.GetTypeInfo().AsType(), type, lifetime));
                }
            }     

            IServiceOperationList serviceOperations = new ServiceOperationList();

            var ignoredServices = _config.GetSection("IgnoredServices").GetChildren();
            var timeout = Convert.ToInt32(_config.GetSection("ServiceTimeout").Value);

            if(timeout == 0)
            {
                _logger.LogWarning("Configuration property \"ServiceTimeout\" not set or equals to 0.");
            }

            var proxies = new List<Type>();
            
            foreach(var contract in contracts)
            {
                if(!ignoredServices.Where(x => x.Value == contract.FullName).Any())
                {
                    var type = contract.GetTypeInfo().AsType();
                    var implementations = new List<Type>();
                    foreach(var assembly in assemblies)
                    {
                        implementations.AddRange(assembly.Value.DefinedTypes.Where(x => contract.IsAssignableFrom(x.AsType()) 
                            && x.IsClass).Select(y => y.AsType()).ToList());
                    }
                    
                    if(implementations.Count > 1) {
                        throw new ArgumentException($"More than one implementation found for interface {contract.FullName}: " 
                            + $"{implementations.Select(x => x.FullName).Aggregate((i, j) => i + "; " + j)}");
                    }

                    serviceOperations.AddRange(GetServiceOperationList(type, contract.GetMethods(), timeout));

                    if(implementations.Count == 1)
                    {
                        serviceCollection.AddScoped(type, implementations.First().GetTypeInfo().AsType());
                    }
                    else {
                        proxies.Add(type);
                        _logger.LogInformation($"Creating CallProxy for interface {contract.FullName}");
                    }
                }
            }

            serviceCollection.AddSingleton<IServiceOperationContainer>(serviceOperations.GetContainer());       

            var container = serviceCollection.BuildServiceProvider();

            var proxy = container.GetRequiredService<ICallProxy>();

            if(useTrustedUser)
            {
                proxy.SetUserId(Constants.TrustedUser);
            }

            foreach(var proxyType in proxies)
            {
                var proxyInstance = proxy.CreateProxy(proxyType);
                serviceCollection.AddSingleton(proxyType, proxyInstance);
            }

            return serviceCollection;
        }

        private static IServiceOperationList GetServiceOperationList (Type type, MethodInfo[] methods, int timeout)
        {
            var serviceOperations = new ServiceOperationList();

            foreach(var method in methods)
            {
                var operationContract = (OperationContractAttribute)method.GetCustomAttribute(typeof(OperationContractAttribute));
                
                if(operationContract != null)
                {
                    var localTimeout = timeout;

                    var serviceTimeout = method.GetCustomAttribute(typeof(ServiceTimeoutAttribute));
                    if(serviceTimeout != null) {
                        localTimeout = ((ServiceTimeoutAttribute)serviceTimeout).Timeout;
                    }
                    
                    var serviceOperation = new ServiceOperationItem 
                    {
                        Interface = type,
                        Method = method,
                        Timeout = localTimeout,
                        IsOneWay = operationContract.IsOneWay,
                        MethodName = operationContract.Name
                    };
                    serviceOperations.Add(serviceOperation);
                }
            }

            return serviceOperations;
        }

        public static IServiceCollection WithLoggerProvider(this IServiceCollection serviceCollection, ILoggerProvider loggerProvider)
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(loggerProvider);
            var logger = loggerFactory.CreateLogger("Distributed.Common");
            _logger = logger;

            serviceCollection.AddSingleton<ILogger>(logger);
            serviceCollection.AddSingleton<ILoggerFactory>(loggerFactory);
            serviceCollection.AddSingleton<ILoggerProvider>(loggerProvider);

            return serviceCollection;
        }

        public static IServiceCollection WithLoggerFactory(this IServiceCollection serviceCollection, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger("Distributed.Common");
            _logger = logger;

            serviceCollection.AddSingleton<ILogger>(logger);
            serviceCollection.AddSingleton<ILoggerFactory>(loggerFactory);
            
            return serviceCollection;
        }
        public static IServiceCollection WithLogger(this IServiceCollection serviceCollection, ILogger logger)
        {
            _logger = logger;
            serviceCollection.AddSingleton<ILogger>(logger);
            return serviceCollection;
        }

        public static IServiceCollection WithConfig(this IServiceCollection serviceCollection, IConfigurationRoot config)
        {
            _config = config;
            serviceCollection.AddSingleton<IConfigurationRoot>(config);
            return serviceCollection;
        }
    }
}