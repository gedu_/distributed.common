using System;
using System.Threading.Tasks;
using Amqp;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace Distributed.Common
{
    public class AmqpMessengerClient : AmqpMessengerBase, IMessengerClient
    {
        private IServiceIdsContainer _serviceIds;
        private IServiceOperationContainer _serviceContainer;
        private Guid _userId;
        public AmqpMessengerClient(IConfigurationRoot config, ILogger logger, IServiceIdsContainer serviceIds,
            IServiceOperationContainer serviceContainer) : base(config, logger)
        {
            _serviceIds = serviceIds;
            _userId = Guid.Empty;      
            _serviceContainer = serviceContainer;     
        }

        public string GetOperationContractName(MethodInfo method)
        {
            return _serviceContainer.GetItem(method).MethodName;
        }

        public MessageResponse ReceiveTo(string queue, Guid correlationId, int timeout, string session)
        {
            CheckConnectivity();
            CheckSession(session);

            var receiver = new ReceiverLink(_sessions[session], queue, queue);
            var response = receiver.Receive(timeout);

            if(response != null)
            {
                var messageResponse = response.GetBody<MessageResponse>();

                if(messageResponse.CorrelationId != correlationId)
                {
                    receiver.Reject(response);
                    _logger.LogError($"Correlation ids do not match for request to [{messageResponse.Queue}]: sent - [{correlationId}], received - [{messageResponse.CorrelationId}]");
                    return null;
                }

                receiver.Accept(response);
                messageResponse.Object = DeserializeType(Type.GetType(messageResponse.Type), messageResponse.Object);
                receiver.Close();

                _sessions[session].Close();

                return messageResponse;
            }

            return null;
        }

        //TODO: check if used 
        public async Task<MessageResponse> ReceiveToAsync(string queue, Guid correlationId, int timeout, string session)
        {
            await CheckConnectivityAsync();
            await CheckSessionAsync(session);

            var receiver = new ReceiverLink(_sessions[session], queue, queue);
            var response = await receiver.ReceiveAsync(timeout);

            if(response != null)
            {
                var messageResponse = response.GetBody<MessageResponse>();

                if(messageResponse.CorrelationId != correlationId)
                {
                    receiver.Reject(response);
                    _logger.LogError($"Correlation ids do not match for request to [{messageResponse.Queue}]: sent - [{correlationId}], received - [{messageResponse.CorrelationId}]");
                    return null;
                }

                receiver.Accept(response);
                messageResponse.Object = DeserializeType(Type.GetType(messageResponse.Type), messageResponse.Object);

                await receiver.CloseAsync();
                await _sessions[session].CloseAsync();
                
                return messageResponse;
            }
            
            return null;
        }

        public void Send(MessageRequest request)
        {
            CheckConnectivity();
            CheckSession(request.Interface);

            var sender = new SenderLink(_sessions[request.Interface], $"Sender_{Guid.NewGuid()}", request.Queue);

            try {

                request.UserId = _userId;

                if(_serviceIds.IsServiceIdsUsed())
                {
                    if(!_serviceIds.ContainsKey(request.Interface))
                    {
                        throw new ArgumentException($"ServiceIds does not contain key {request.Interface}.");
                    }

                    request.ServiceId = _serviceIds[request.Interface];
                }

                var message = new Message(request);

                _logger.LogDebug($"Sending request to queue {request.Queue} [CorrelationId: {request.CorrelationId}]");

                sender.Send(message);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error sending request to queue {request.Queue} [CorrelationId: {request.CorrelationId}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            sender.Close();

            _sessions[request.Interface].Close();
        }

        //TODO: check if used
        public async Task SendAsync(MessageRequest request)
        {
            await CheckConnectivityAsync();
            await CheckSessionAsync(request.Interface);

            var sender = new SenderLink(_sessions[request.Interface], $"Sender_{Guid.NewGuid()}", request.Queue);

            try {

                request.UserId = _userId;  

                if(_serviceIds.IsServiceIdsUsed())
                {
                    if(!_serviceIds.ContainsKey(request.Interface))
                    {
                        throw new ArgumentException($"ServiceIds does not contain key {request.Interface}.");
                    }
                    
                    request.ServiceId = _serviceIds[request.Interface];
                }

                var message = new Message(request);

                _logger.LogDebug($"Sending request to queue {request.Queue} [CorrelationId: {request.CorrelationId}]");

                await sender.SendAsync(message);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error sending resquest to queue {request.Queue} [CorrelationId: {request.CorrelationId}]");
                _logger.LogError($"Exception: {e}");
                _logger.LogError($"{e.Message}");
            }

            await sender.CloseAsync();
            await _sessions[request.Interface].CloseAsync();
        }

        public void SetUserId(Guid userId)
        {
            _userId = userId;
        }
    }
}