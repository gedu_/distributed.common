namespace Distributed.Common.Enums
{
    public enum ServiceOverride
    {
        None,
        Library,
        User
    }
}